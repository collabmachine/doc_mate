
# Notes sur l'utilisation du moteur de documentation

Ruby on Rails engine to build a standardized documentation display out of markdown files found in the public/docs directory.




# Concept général


- Il s'agit d'inclure le doc_mate dans un projet rails (A) pour profiter d'une mécanique qui construit une documentation à partir de fichiers markdown que l'on place dans public/docs du projet (A).

# Les titres

## Utiliser les titres réguliers dans la documentation.

    # niveau h1
    ## niveau h2
    ### niveau h3
    #### niveau h4
    ##### niveau h5



Pour générer une table des matières automatique à partir des titres

h1,h2,h3...

    * toc
    {:toc}


Pour éviter qu'un titre se retrouve dans la table des matières:

    # Un titre ici
    {:.no_toc}


## Deep linking urls

Le processing des titre génère des anchors. Il est donc possible de créer des liens qui mènent directement vers la section spécifiée.

~~~
    # Le nom -------&gt; &lt;h2 id=&quot;le-nom&quot;&gt;Le nom&lt;/h2&gt;

    url:
    http://__domain__#le-nom
~~~

On peut faire des includes de fichiers markdown en utilisant la syntaxe:


~~~
    &lt;&lt;{filename_to_include.md}
~~~


On peut inclure récursivement le contenu

- Fichier A
    - Fichier A inclut Fichier x
        + fichier x inclut Fichier y
        + ...


Utiliser des marqueurs pour limiter l'affichage

~~~
<!-- scope:admin -->...<!-- end-scope:admin -->
<!-- scope:private -->...<!-- end-scope:private -->
~~~







Sidebar
- keyboard friendly



# Todos :
- improve default file recognition
    + index, readme, toc, table-des-matieres
    + extentions : .md, markdown...
- ajouter syntax coloration auto. (code blocks)

