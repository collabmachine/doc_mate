require_dependency "doc_mate/application_controller"

module DocMate
  class DocsController < ApplicationController
    layout 'doc_mate/docs'

    require 'kramdown'

    def index
      # toc_file = [level,'index.md'].join('-')
      toc_file = 'doc-root.markdown'
      content = ''
      content = recursive_get_content(toc_file, content)

      ## Strip the undesired content based on access level
      # content.gsub!(/((<!--\s?scope:admin\s?-->(.*?)<!--\s?end-scope:admin\s?-->))/m, '')
      content = restrict_to_level content
      @content = Kramdown::Document.new(content).to_html.html_safe
    end

    def show
    end

    def file
      file = params[:id]
      desired_format = params[:format]
      content = File.read("#{Rails.public_path}/docs/#{file}.#{desired_format}")
      render plain: content
    end

    private


    def restrict_to_level(content)
      case level
      when 'admin' # show everything
      when 'private'
        # remove admin content
        content.gsub!(/(<!--\s?scope:\b(?:admin)\b\s?-->(.*?)<!--\s?end-scope:\b(?:admin)\b\s?-->)/m, '')
      else
        #public: remove private + admin content
        # (<!--\s?scope:\b(?:admin|private)\b\s?-->(.*?)<!--\s?end-scope:\b(?:admin|private)\b\s?-->)
        content.gsub!(/(<!--\s?scope:\b(?:admin)\b\s?-->(.*?)<!--\s?end-scope:\b(?:admin)\b\s?-->)/m, '')
        content.gsub!(/(<!--\s?scope:\b(?:private)\b\s?-->(.*?)<!--\s?end-scope:\b(?:private)\b\s?-->)/m, '')
      end
      content
    end

    def level
      level = 'public'
      if current_user
        level = case current_user.access_level
          when 'member' then 'private'
          when 'admin' then 'admin'
          else 'public'
        end
      end
      level
    end


    def recursive_get_content(filename, content)
      content = File.read("#{Rails.public_path}/docs/#{filename}")
      content.scan(%r{<<\{([a-zA-Z0-9\-_\/]+.[a-zA-Z0-9\-_]+)\}}) do |fname,dummy|
        content = content.gsub(%r{<<\{#{fname}\}}, recursive_get_content(fname, content))
      end
      return content
    end

  end
end
