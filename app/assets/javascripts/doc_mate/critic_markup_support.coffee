$(document).on "ready", ->

  # /\{~~(.*)~>(.*)~~}/g
  # -- sort comme '–'(tiret long) dans le html

  replace_in_document = (rule) ->
    $("body").html $("body").html().replace(rule.critic_marker, rule.html_replacement)


  # init, replace critic markup by html tags indocument content
  handle_critic_classes = ->
    critic_markup_mode = localStorage.critic_markup_mode or critic_markup_mode or "visible"
    $(".wrap-critic-markup-controls a").removeClass "on"
    switch critic_markup_mode
      when "visible"
        $("body").removeClass "critic-markup-processed"
        $(".wrap-critic-markup-controls a.set-visible").addClass "on"
      when "processed"
        $("body").addClass "critic-markup-processed"
        $(".wrap-critic-markup-controls a.set-processed").addClass "on"

  markers = [
    critic_type: "substitution"
    critic_marker: /\{~~(.*)~&gt;(.*)~~}/g
    html_replacement: "<span class=\"critic sub\"><span class=\"initial\">$1</span><span class=\"tilde-right-arrow\">~></span><span class=\"canditate\">$2</span></span>"
  ,
    critic_type: "comment-mark"
    critic_marker: /\{==(.*)==}/g
    html_replacement: "<span class=\"critic highlight\"><mark>$1</mark></span>"
  ,
    critic_type: "comment-mark"
    critic_marker: /\{»(.*)«}/g
    html_replacement: "<span class=\"critic comment\"><span class=\"critic comment\">$1</span></span>"
  ,
    critic_type: "addition"
    critic_marker: /\{\+\+(.*)\+\+}/g
    html_replacement: "<span class=\"critic insert\"><ins>$1</ins></span>"
  ,
    critic_type: "deletion"
    critic_marker: /\{–(.*)–}/g
    html_replacement: "<span class=\"critic delete\"><del>$1</del></span>"
   ]

  $.each markers, (i, item) ->
    replace_in_document item

  $(".wrap-critic-markup-controls a").click (e) ->
    e.preventDefault()
    value = undefined
    if $(this).hasClass("set-visible")
      value = "visible"
    else value = "processed"  if $(this).hasClass("set-processed")
    memory.keep "critic_markup_mode", value
    handle_critic_classes()

  handle_critic_classes()
