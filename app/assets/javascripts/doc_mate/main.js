(function($) {
  'use strict';
  var

    Menu = function(element, window, options) {
      var $elem = $(element),
        $tree = $('#tree') || 0,
        menu = this,
        track = {
          scroll_position: 0
        };

    menu.init = function() {
      track.scroll_position = parseInt(memory.read('scroll_position') || track.scroll_position || 0);

      menu.bind_fancy_tree();
      if ($tree.length !== 0) {
        menu.handle_menu_state();
        menu.bind_events();
      }

    };
    menu.bind_fancy_tree = function() {
      var $table_of_content = $('#markdown-toc');

      if ($tree.length === 0) {
        if (location.hash.indexOf('#keep-toc') != -1){
          $table_of_content = $('#markdown-toc').clone();
        }
        $table_of_content.appendTo('#main_menu').wrap('<div id="tree"></div>');
        $tree = $('#tree');
        var nav_controls = $('<div>', {
          'class': 'nav-controls'
        }).append([
          $('<a/>', {
            'class': 'expand-all top',
            'html': 'Expand all'
          }),
          $('<a/>', {
            'class': 'collapse-all top',
            'html': 'Collapse all'
          })
        ]);
        $('#main_menu').prepend(nav_controls);
        // .append(nav_controls.clone());
      }

      $tree.fancytree({
        autoActivate: false,
        minExpandLevel: 1,
        collapse: function(event, data) {
          menu.set_wrapper_margin();
        },
        expand: function(event, data) {
          menu.set_wrapper_margin();
        },
        postinit: function(isReloading, isError) {
          this.reactivate();
        },
        focus: function(event, data) {
          // Auto-activate focused node after 2 seconds
          // data.node.scheduleAction("activate", 2000);
        },
        activate: function(event, data) {
          var node = data.node;
          if (node.data.href) {
            window.location.href = node.data.href;
            if($('body').hasClass('mobile')){
              menu.set_menu_state('off');
            }
          }
        }
      });
    };
    menu.set_expansion = function(state) {
      var tree = $tree.fancytree('getTree');
      if (state === 'on') {
        tree.visit(function(node) {
          node.setExpanded(true);
        });
        menu.set_wrapper_margin();
        memory.keep('menu_expanded', 'on');
      } else {
        tree.visit(function(node) {
          node.setExpanded(false);
        });
        menu.set_wrapper_margin();
        memory.keep('menu_expanded', 'off');
      }
    };
    menu.handle_menu_state = function() {
      if (memory.read('menu_expanded') === 'on') {
        menu.set_expansion('on');
      } else {
        menu.set_expansion('off');
      }

      // $elem[0].scrollIntoView(track.scroll_position);

      if (memory.read('menu_pinned') === 'on') {
        menu.set_menu_state('on');
        menu.pin_menu('on');
      } else {
        menu.set_menu_state('off');
        menu.pin_menu('off');
      }
    };
    menu.bind_events = function() {
      $('.expand-all').click(function() {
        menu.set_expansion('on');
      });
      $('.collapse-all').click(function() {
        menu.set_expansion('off');
      });

      $elem.bind('mouseenter mouseleave', function(event) {
        if ($('.pin').hasClass('on')) {
          return false;
        }
        if ($elem.hasClass('open')) {
          menu.set_menu_state('off');
        } else {
          menu.set_menu_state('on');
        }
        event.preventDefault();
      });

      $('.toggle-menu').click(function(event){
        event.preventDefault();
        if ($(this).parent('#control_bar').hasClass('open')) {
          menu.set_menu_state('off');
        } else {
          menu.set_menu_state('on');
        }
      });

      $('.pin').click(function(event) {
        $(this).toggleClass('on');
        if ($(this).hasClass('on')) {
          menu.pin_menu('on');
        } else {
          menu.pin_menu('off');
        }
        event.preventDefault();
      });

      // keep track of the left menu scrolling position
      $elem.scroll(function() {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function() {
          track.scroll_position = $elem.scrollTop();
          memory.keep('scroll_position', track.scroll_position);
        }, 250));
      });

    };
    // true / false
    menu.set_menu_state = function(state) {
      if (state === 'on') {
        $elem.switchClass('', 'open', 300).promise().done(function() {
          memory.keep('menu_state_opened', 'on');

          if($('body').hasClass('desktop')){
            menu.set_wrapper_margin();
          }

        });
      } else {
        $elem.switchClass('open', '', 300).promise().done(function() {
          memory.keep('menu_state_opened', 'off');

          if($('body').hasClass('desktop')){
            menu.set_wrapper_margin();
          }

        });
      }
    };
    // true / false
    menu.pin_menu = function(state) {
      if (state === 'on') {
        $('.pin').addClass('on');
        memory.keep('menu_pinned', 'on');
      } else {
        $('.pin').removeClass('on');
        memory.keep('menu_pinned', 'off');
      }
    };

    menu.set_wrapper_margin = function() {

      var mode = $('body').hasClass('desktop') ? 'desktop': 'mobile',
      css_set = {};

        css_set.desktop = {
          closed: {'margin': '0 3em 0 5em'} ,
          open: { 'margin': '0 3em 0 ' + ($elem.width() + 100) + 'px' }
        },
        // css_set.mobile = { closed: '0 1em 0 1em' , open: '0 1em 0 1em'};
        css_set.mobile = {
          closed: { 'padding': '0 1em 0 1em'} ,
          open: { 'padding': '0 1em 0 1em'}
        };

      var css = css_set[mode].closed;

      // var margin = '0 3em 0 5em';
      if ($elem.hasClass('open')) {
        css = css_set[mode].open;
      }

      $('#wrapper').css(css);
    };

    this.init();
  };

  $.fn.menu = function(options) {
    return this.each(function() {
      var element = $(this),
        menu;
      if (element.data('menu')) {
        return;
      }
      menu = new Menu(this, window, options);
      element.data('menu', menu);
    });
  };

}(jQuery));


Local_storage_interface = function() {
  var
    memory = this;
  memory.init = function() {};
  memory.keep = function(key, value) {
    localStorage[key] = value;
  };
  memory.read = function(key) {
    return localStorage[key];
  };
  this.init();
};
var memory = new Local_storage_interface();


function detect_agent(){
  return navigator.userAgent.match( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i) || false;
}

// -_-_-_- document ready -_-_-_-
$(document).on('ready', function() {
  $('body').addClass(detect_agent() ? 'mobile':'desktop');
  $('#control_bar').menu();
});
