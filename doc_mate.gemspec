$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "doc_mate/version"


# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "doc_mate"
  s.version     = DocMate::VERSION
  s.authors     = ["PL"]
  s.email       = ["pl@roostermotion.com"]
  s.homepage    = "http://roostmotion.com"
  s.summary     = "DocMate..."
  s.description = "Description of DocMate."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency 'rails'
  s.add_dependency 'sass-rails'
  s.add_dependency 'font-awesome-sass'
  s.add_dependency 'uglifier'
  s.add_dependency 'jquery-ui-rails'
  s.add_dependency 'coffee-rails'
  s.add_dependency 'turbolinks'
  s.add_dependency 'kramdown'


  s.add_development_dependency 'sqlite3'
end
