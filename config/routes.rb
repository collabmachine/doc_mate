DocMate::Engine.routes.draw do
  resources :docs

  get 'file/:id' => "docs#file"

  root to: "docs#index"
end
