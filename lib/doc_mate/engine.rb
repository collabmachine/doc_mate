module DocMate
  class Engine < ::Rails::Engine
    isolate_namespace DocMate
    require 'rubygems'
    require 'jquery-ui-rails'
    require 'kramdown'

    config.assets.paths << File.expand_path("../../assets/stylesheets", __FILE__)
    config.assets.paths << File.expand_path("../../assets/javascripts", __FILE__)

    config.assets.precompile += %w( doc_mate.css )

    initializer "static assets" do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/public"
    end

  end
end
